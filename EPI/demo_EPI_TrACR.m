function demo_EPI_TrACR()

addpath ../util/

% EPI-Trajectory Auto-Corrected Reconstruction (TrACR) for EPI trajectories
% using SENSE or SPIRiT image reconstructions
%
% demo for 2-shot 2x accelerated EPI dataset
%
% parameters~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
p.thresh=1e-6;      %cost change threshold at which to cut off TrACR
p.numshot=2;        %number of EPI shots
p.acc=2;            %acceleration factor *MUST be integer for segmented FFTs!*
p.dim=[160 160];    %desired reconstructed image matrix size (actual dim
%                       may change w/in TrACR for piecewise/segmented
%                       FFTs)
p.fov=[24 24];      %field of view in cm [N x M]
p.tol=1e-1;         %tolerance for cutting off lsqr for image recons
p.niterk=5;         %max # of iterations per k-space update  (5)
p.niteri=25;        %max # of iterations per image update
p.niterphi=5;       %max number of DC phase updates per iter
%
%
%options~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
calibrated_init=0;  %whether to start w/the initial estimates
p.SegFFTs=1;        %1 if using piecewise/segmented FFTs, 0 or not included if not.
p.trunc=48;         %(optional) desired truncation of data in 2D for speed (as number of
%                   PE lines remaining)
p.reconalg='SENSE'; %image reconstruction type
p.DCphase=1;        %whether to include DC phase errors in updates
%p.useGmri=1        %1 if using NUFFT/Gmri, 0 or not included if not.
%
% data ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
fname='demo_EPI_data.mat'; %data file containing...
%   data_EPI    - data [squeeze(n_FE x n_PE x n_coils)] expected in regular
%                   Cartesian ordering (no fliplr of PE lines)
%   knom        - nominal trajectory [n_FE x n_PE] in cycles/cm (kx + 1i * ky)
%   SENSEmap    - coil sensitivities [(n_pts * n_proj * n_coils) x n_slice]
%   cinit1      - optional, pass in an initial estimate of basis weights
%                   (calculated from calibration data), expected as:
%                   [4*Nshot x 1] (shot 1 odd, shot 2 odd, shot 1
%                   even,etc). Traj. delays come before phi estimates.
%
% NOTE, 1st dimension is assumed to be phase encode.
%% load in data
fprintf('loading data...\n');
load(fname);
if ~calibrated_init
    clear cinit1;
end
%% set up some parameters
p.datadims=size(data_EPI);
p.datadims=p.datadims(1:2);
%get data dimensions: knom is n_pts x n_proj
npts = (size(knom,1));    % number of points per PE line
nproj= (size(knom,2));    % number of PE lines

data=data_EPI;
%% reshape/zeropad data if using Gmri_epi (SegFFTs):
dimorig=p.dim; %keep track
if isfield(p,'SegFFTs') && p.SegFFTs
    [knom,p,nproj,npts,data,diffdim]=SEGFFT_reshape_zero(p,data,knom);
end
%% apply truncation to data if applicable:
if isfield(p,'trunc') && p.trunc
    [p,data,knom,nproj,npts,SENSEmap,dim2]=applyTrunc(p,data,knom,nproj,npts,dimorig,SENSEmap);
end
%% reshape data to common form, & 1D-IFT if using SegFFTs
data_EPI=reshape(data,[],size(data_EPI,3));
% data is now [ (nproj*nline) x ncoils ]

%if using Gmri_epi, do 1D IFT in readout now:
if isfield(p,'SegFFTs') && p.SegFFTs
    data=ifftshift(ifft(ifftshift(reshape(data_EPI,[p.datadims size(data_EPI,2)]),1),[],1),1)*sqrt(p.datadims(1));
    data_EPI=reshape(data,[],size(data_EPI,2));
end
% if truncating, do same thing for non-truncated data, (for final recon)
if isfield(p,'t')
    p.t.data=reshape(p.t.data,[],size(p.t.data,3));
    % data is now [ (nproj*nline) x ncoils ]
    %if using Gmri_epi, do 1D IFT in readout now:
    if isfield(p,'SegFFTs') && p.SegFFTs
        data1=ifftshift(ifft(ifftshift(reshape(p.t.data,[p.t.datadims size(p.t.data,2)]),1),[],1),1)*sqrt(p.t.datadims(1));
        p.t.data=reshape(data1,[],size(p.t.data,2));
    end
end

%% set up more TrACR parameters
p.data = reshape(double(data_EPI),[],size(data_EPI,2));   % k-space data in
p.ncoils = size(data_EPI,2); % # Rx coils
p.knom = [real(knom(:)) imag(knom(:))];   % nominal k-space trajectory, cycles/cm [# points per line x # lines]
p.maxiters = 1e5;  % maximum # of outer iterations to run TrACR
if exist('SENSEmap')
    p.SENSEmap = SENSEmap;    % Rx sensitivity maps for SENSE recon
end
if ~isfield(p,'wi') %set up data weighting
    p.wi=ones(size(p.data(:,1)));
end
if ~isfield(p,'tol')
    p.tol=1e-1;
end
%% normalize the sensitivity maps:
% (makes lsqr tolerance meaningful to compare between recons)
if exist('SENSEmap','var')
    if ~exist('dim2','var')
        dim2=[dimorig size(data_EPI,2)];
    end
    [smap,sensemap]=normalizesens(p,SENSEmap,dim2);
    p.SENSEmap=smap;
    if isfield(p,'trunc')
        [smapt,sensemapt]=normalizesens(p,p.t.SENSEmap,[p.t.dim size(data_EPI,2)]);
        p.t.SENSEmap=smapt;
    end
end
%% if using piecewise/segmented FFTs, reinterpolate SENSEmap to fit the image:
if isfield(p,'SegFFTs') && p.SegFFTs
    p.SENSEmap=reinterp_sens(data,dim2,sensemap,p.SENSEmap);
    if isfield(p,'trunc')
        p.t.SENSEmap=reinterp_sens(data1,[p.t.dim size(data_EPI,2)],sensemapt,p.t.SENSEmap);
    end
end
%% create some dummy variables on each core
% for FFT operators, so that they will work in parfor loops within lsqr.
%alternatively, could remove parfors but that's slow and no fun.
parfor ii=1:feature('numCores')
    if isfield(p,'SegFFTs') && p.SegFFTs
        GmriEPIdummy=Gmri_epi(true([2 2]),true([2 2]),0,0);
    elseif isfield(p,'useGmri')
        nufftdummy = Gmri(zeros([3 2]),true(3),'fov',[3 3]);
    else
        nufftdummy = gg_nufft(zeros([2 2]),3,6,2);
    end
end

%% FFT parameters----------------------------------------------------------
if isfield(p,'useGmri') && p.useGmri
    p.Msp = 6; % spreading parameter
    p.R = 2; % oversampling ratio
end

% set up image grid locations - used in k-space derivative calculation
[x,y] = ndgrid(-p.fov/2+p.fov./p.dim(1):p.fov./p.dim(1):p.fov/2, -p.fov/2+p.fov./p.dim(2):p.fov./p.dim(2):p.fov/2);
p.x = x(:);
p.y = y(:);
%% set up k-space error basis
p=setupbasis(p,nproj,npts);

%% Set up DC phase error basis
if isfield(p,'DCphase');
    p=setupphibasis(p,nproj,npts);
end

%% accept an initial value of the basis function weights (default is zeros)
if exist('cinit1','var') && ~isempty(cinit1)
    p=initweights(p,cinit1);
end

%% for piecewise FFTs: fix ordering of init estimates from zero-padding the shot dim
if isfield(p,'SegFFTs') && p.SegFFTs && isfield(p,'cinit')
    p=fix_init_estimate_order(p,diffdim);
end
%% run TrACR----------------------------------------------------------------

[img,imginit,c,cost,imgsv,csv,p] = EPI_TrACR(double(p.data),p);
fprintf(['EPI-TrACR finished with: %4.0f iterations. \n ', ...
    'Total compute time: %3.0f minutes %3.0f seconds\n'], ...
    size(cost,2)-1,p.runtime/60, rem(p.runtime,60));
%% display results-----------------------------------------------------------
img_initial = sqrt(sum(abs(imginit).^2,3)).';
img_final = sqrt(sum(abs(img).^2,3)).';

figure; subplot(2,2,1); imagesc(img_initial,[0 .2*max([img_initial(:); img_final(:)])]);
colormap gray; title('initial sum-of-squares image'); axis off; axis image;
subplot(2,2,2); imagesc(img_final,[0 .2*max([img_initial(:); img_final(:)])]);
colormap gray; title('final sum-of-squares image'); axis off; axis image;
subplot(2,2,3); imagesc(.2*abs(img_final-img_initial));
colormap gray; title('difference'); axis off; axis image;
subplot(2,2,4); semilogy(cost); xlabel('iteration #'); ylabel('data cost term');
title('cost');
fprintf('final estimates:\n');
disp(c);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set up k-space error basis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [p]=setupbasis(p,nproj,npts)
fprintf('Setting up error basis...\n');

p.eb=[];
selection=zeros(nproj,npts);
selection(1:p.numshot*2:end,:)=1;
for shotnum=1:p.numshot*2
    selectionnow=circshift(selection,shotnum-1);
    selectionnow(1:shotnum-1,:)=0;
    selectionnow=logical(selectionnow);
    selectionnow=permute(selectionnow,[2 1]);
    p.eb(:,end+1)=[selectionnow(:); zeros(size(selectionnow(:)))]; %separate per shot...
end
p.eb(:,1)=[]; %nix to get rid of too many deg of freedom

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set up phase error basis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function p=setupphibasis(p,nproj,npts)
% phi should look just like k-basis
% separate odd and even lines...

%for multishot:
p.ebp=[];
selection=zeros(nproj,npts);
selection(1:p.numshot*2:end,:)=1;
for shotnum=1:p.numshot*2
    selectionnow=circshift(selection,shotnum-1);
    selectionnow(1:shotnum-1,:)=0;
    selectionnow=logical(selectionnow);
    selectionnow=permute(selectionnow,[2 1]);
    p.ebp(:,end+1)=[selectionnow(:)];
end
p.ebp(:,1)=[]; %nix to get rid of so many deg of freedom.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% reshape/zeropad data if using Gmri_epi (SegFFTs):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [knom,p,nproj,npts,data,diffdim]=SEGFFT_reshape_zero(p,data,knom)
% handle data/image dimensions such that segmented FFTs are
%   happy
%if this is an p.accelerated scan, treat zero-ed lines as
%uncollected shots.
if p.acc>1
    p.numshot=p.numshot*p.acc;
    %introduce shot mask so that Gmri_epi won't calc FT of these
    %extra shots:
    shotmask=zeros(p.numshot,1);
    shotmask(1:p.acc:end)=1;
    p.shotmask=logical(shotmask);
    %add zero data p.accordingly (to the new "shots").
    sizedat=size(data);
    sizedat(2)=p.acc*sizedat(2);
    data2=zeros(sizedat);
    data2(:,1:p.acc:end,:,:,:)=data;
    data=data2;
    %assign weights p.wi to correspond.
    %set the non-acquired line weights to zero.
    p.wi=zeros(size(data,1), size(data,2));
    p.wi(:,1:p.acc:end)=1;
    %reset data size.
    p.datadims=size(data);
    p.datadims=p.datadims(1:2);
    %interpolate k-space appropriately...
    orig_k_ind=1:p.acc:size(knom,2)*p.acc;
    new_k_ind=1:orig_k_ind(end);
    orig_k_ind1=1:size(knom,1);
    [oki1,oki]=meshgrid(orig_k_ind1,orig_k_ind);
    [oki2,nki]=meshgrid(orig_k_ind1,new_k_ind);
    knew=interp2(oki1,oki,knom.',oki2,nki);
    knom=knew.';
    %add on the last [p.acc] lines of knom that don't get
    %interp'd properly
    deltak=imag((knom(1,end)-knom(1,end-1)));
    for accLine=1:p.acc-1
        knom(:,end+1)=knom(:,end)+1i.*deltak;
    end
end
%change image dims to match data..and meet req of
%     Gmri_epi that rem(2nd dim,2*numshots)==0
p.dim(2)=p.datadims(2);
%make sure Gmri_epi will p.accept dims (fac must be integer)
fac= p.dim(2)/(2*p.numshot); %number of lines per shot/even-odd combo
%...and that we don't have problem w/TrACR (?) (fac must be
%even)
while round(fac)~=fac || (rem(fac,2)~=0)
    p.dim(2)=p.dim(2)+1;
    fac= p.dim(2)/(2*p.numshot);
end
dsize=size(data);
p.datadims=dsize(1:2); %update data dimensions
% set first image dim to size of 1st data dim so we don't have
% to wrangle data more (and bc it doesn't matter)
p.dim(1)=p.datadims(1);
%~~zero-pad data in 2nd dim so that it meets dim reqs~~
diffdim=abs(p.datadims(2)-p.dim(2)); %determine zero-pad size
% flag if zero-pad size is odd:
if rem(diffdim,2)==1
    oddflag=1;
else
    oddflag=0;
end
ddimsall=size(data);
ddimpad = ddimsall;
ddimpad(2)=floor(diffdim/2);
data=cat(2,zeros(ddimpad),data);
if isfield(p,'wi') %assign extra data zero weight
    p.wi=cat(2,zeros(ddimpad(1:2)),p.wi);
end
if oddflag
    extradim = zeros(size((ddimpad)));
    extradim(2)=1;
    data=cat(2,data,zeros(ddimpad+extradim));
    if isfield(p,'wi') %assign extra data zero weight
        p.wi=cat(2,p.wi,zeros(ddimpad(1:2)+extradim(1:2)));
    end
else
    data=cat(2,data,zeros(ddimpad));
    if isfield(p,'wi') %assign extra data zero weight
        p.wi=cat(2,p.wi,zeros(ddimpad(1:2)));
    end
end
if isfield(p,'wi') %assign extra data zero weight
    p.wi=p.wi(:);
end
%recompute data dims:
p.datadims=size(data);
p.datadims=p.datadims(1:2);
% pad k-space p.accordingly:
kdim = size(knom);
kdimpad = kdim;
kdimpad(2)=floor(diffdim/2);
deltak=imag((knom(1,end)-knom(1,end-1)));
realkpadL = repmat(real(knom(:,1)),1,kdimpad(2));
if oddflag
    realkpadR = repmat(real(knom(:,1)),1,kdimpad(2)+1);
else
    realkpadR=realkpadL;
end
imagkpadL = repmat(imag(knom(1,1))-deltak:-deltak:(imag(knom(1,1))-(deltak*kdimpad(2))),kdimpad(1),1);
if oddflag
    imagkpadR= repmat(imag(knom(1,1))-deltak:-deltak:(imag(knom(1,1))-(deltak*(kdimpad(2)+1))),kdimpad(1),1);
else
    imagkpadR = imagkpadL;
end
kpad = realkpadL+1i*fliplr(imagkpadL);
knom=cat(2,kpad,knom);
kpad2=realkpadR-1i*(imagkpadR);
knom=cat(2,knom,kpad2);
nproj=size(knom,2);
npts=size(knom,1);
p.knom=[real(knom(:)) imag(knom(:))];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% apply truncation to data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [p,data,knom,nproj,npts,SENSEmap,dim2]=applyTrunc(p,data,knom,nproj,npts,dimorig,SENSEmap)
%save orig dims for final recon
dim2=dimorig;
t.data=double(data);
t.knom=knom;
t.nproj=nproj;
t.npts=npts;
t.datadims=p.datadims;
t.dim=t.datadims;
if isfield(p,'wi')
    t.wi=p.wi;
else
    t.wi=ones(size(p.data(:,1)));
    t.wi=t.wi(:);
end

if isfield(p,'useGmri') && p.useGmri
    error('Not set up to truncate w/NUFFTs');
end
red_dim=p.trunc; %number of lines to keep (in first dim)
if rem(red_dim,2)==1 %if this is odd data is misalligned
    red_dim=red_dim+1;
end
startind=round((p.datadims(1)-red_dim)/2)+1;
endind=startind+red_dim-1;
%2nd dim; do this such that every line is same type as
%before (even/odd, shot #)
red_dim2=p.trunc; %number of lines to keep (in 2nd dim)
if rem(red_dim2,2)==1
    red_dim2=red_dim2+1;
end
startind2=round((p.datadims(2)-red_dim2)/2)+1;
while(rem(startind2,2*p.numshot)~=1)
    startind2=startind2-1;
end
endind2=startind2+red_dim2-1;

%cut the data by specified factor along both dims:
%make sure Gmri_epi will p.accept dims (fac must be integer)
fac= length(startind2:endind2)/(2*p.numshot); %number of lines per shot/even-odd combo
%...and that we don't have problem w/TrACR (fac must be
%even)
while round(fac)~=fac || (rem(fac,2)~=0)
    endind2=endind2+1;
    fac= length(startind2:endind2)/(2*p.numshot);
end
if isfield(p,'wi')
    wi=reshape(p.wi,p.datadims);
    p.wi=wi(startind:endind,startind2:endind2);
    p.wi=p.wi(:);
end
data=data(startind:endind,startind2:endind2,:,:,:);
knom=knom(startind:endind,startind2:endind2);
nproj=size(knom,2);
npts=size(knom,1);
p.knom=[real(knom(:)) imag(knom(:))];
%recompute data dims:
p.datadims=size(data);
p.datadims=p.datadims(1:2);
%also reduce image resolution to match:
p.dim=p.datadims;
%truncate sensitivities too:
if exist('SENSEmap','var')
    t.SENSEmap=SENSEmap;
    sdata=size(data);
    dim2=[dimorig sdata(end)];
    SENSEmap=reshape(SENSEmap,dim2);
    % -->k-space
    for ii=1:size(SENSEmap,3)
        for jj=1:size(SENSEmap,4)
            sk(:,:,ii,jj)=fftshift(fft2(fftshift(SENSEmap(:,:,ii,jj))));
        end
    end
    % truncate sk & go back to im domain:
    sk=sk(startind:endind,startind2:endind2,:,:);
    for ii=1:size(sk,3)
        for jj=1:size(sk,4)
            sm(:,:,ii,jj)=ifftshift(ifft2(ifftshift(sk(:,:,ii,jj))));
        end
    end
    SENSEmap=sm;
    dim2=size(SENSEmap);
    SENSEmap=SENSEmap(:);
end
p.t=t;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% initialize error estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function p=initweights(p,cinit1)

if isfield(p,'SegFFTs') && p.SegFFTs && p.acc>1
    %modify cinit such that our "unacquired shots" don't
    %interfere
    ca=cinit1(1:end/2,:);
    cb=cinit1(end/2+1:end,:);
    ca2=zeros(size(ca,1)*p.acc,size(ca,2));
    cb2=zeros(size(cb,1)*p.acc,size(ca,2));
    ca2(1:p.acc:end,:)=ca;
    cb2(1:p.acc:end,:)=cb;
    cinit1=[ca2;cb2];
end
p.cinit=cinit1;
p.cinit(end/2+1)=[]; %nix to get rid of too many deg of freedom
p.cinit(1)=[]; %nix to get rid of too many deg of freedom

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% reorder initial error estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function p=fix_init_estimate_order(p,diffdim)
ca=[0; p.cinit(1:end/2)];
cb=[0; p.cinit(end/2+1:end)];
shotind=[1:length(cb)];
%shift estimates by # of zeropadded lines ("shots") prepended
%(appended don't matter)
shotdiff=rem(floor(diffdim/2),p.numshot*2);
%BUT if there's not an EVEN number of lines left after dividing
%number of lines (2nd dim) by 2*p.numshot, shotdiff will be off
%by numshots.* SO:
if rem(p.dim(2)/(2*p.numshot),2)==1
    shotdiff=shotdiff-p.numshot;
end
%^this may be redundant since we check for this above. but if
%not...
ca=circshift(ca,shotdiff);
cb=circshift(cb,shotdiff);
shotind=(circshift((shotind(:)),shotdiff));
p.shotID=shotind(2:end); %save out what shot is what.
ca=ca(2:end)-ca(1);
cb=(cb(2:end)-cb(1));
p.cinit=[ca;cb];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% reinterpolate SENSE map to fit new image recon dims
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function SENSEmap=reinterp_sens(data,dim2,sensemap,SENSEmap)
newdims=size(data);
newdims=[newdims(1:2) newdims(end)];
if ~all(dim2==newdims) %check if coil sensitivity data needs to be zero padded
    %FT sens data:
    parfor coilnum=1:dim2(end)
        sft(:,:,coilnum)=fftshift(fft2(fftshift(sensemap(:,:,coilnum))));
    end
    %pad it to the new data/img dims:
    dimpad=newdims-dim2;
    sftpad=zeros(newdims);
    sftpad(dimpad(1)/2+1:end-dimpad(1)/2,dimpad(2)/2+1:end-dimpad(2)/2,:)=sft;
    %IFT and col
    parfor coilnum=1:dim2(end)
        sensemapnormnew(:,:,coilnum)=ifftshift(ifft2(ifftshift((sftpad(:,:,coilnum)))));
    end
    ssos=sqrt(sum(abs(sensemapnormnew.^2),3));
    parfor coilnum=1:dim2(end)
        sensemapnormnew(:,:,coilnum)=sensemapnormnew(:,:,coilnum)./ssos;
    end
    SENSEmap=double(sensemapnormnew(:));
end

function [smap,sensemap]=normalizesens(p,SENSEmap,dim2)
if exist('SENSEmap','var')
    sensemap=reshape(SENSEmap,dim2);
    sossensemap=sqrt(sum(abs(sensemap.^2),3));
    for ii=1:size(sensemap,3);
        sensemapnorm(:,:,ii)=sensemap(:,:,ii)./sossensemap;
    end
    smap=double(sensemapnorm(:));
end