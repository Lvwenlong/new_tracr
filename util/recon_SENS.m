function [img,flag,relres,iter,resvec,lsvec] = recon_SENS(data,imginit,niteri,wi,G,SENSEmap,tol)
% reconstruct an image from non-Cartesian acquisitions, using SENSE

%data       =   initial k-space data (must be a double!)
%imginit    =   initial image
%niteri     =   number of lsqr iterations
%wi         =   density weights
%G          =   NUFFT object (set with Gnufft)
%SENSEmap   =   map of coil sensitivities (must be a double!)

SENSEmap = SENSEmap(:);
N = numel(imginit);     %number of image points
imSize = size(imginit); %image size
dataSize = size(data);  %k-space data size

b = data(:).*repmat(sqrt(wi(:)),[length(data(:))/length(wi(:)) 1]);
wi=sqrt(wi(:));
if ~isvar('tol')
    tol=1e-6;
end
%perform least-squares recon
[img,flag,relres,iter,resvec,lsvec] = lsqr(@(x,tr)afun(x,wi,dataSize,imSize,tr,G,SENSEmap), b, tol, niteri,speye(N,N),speye(N,N), imginit(:));

img = reshape(img,imSize);

function [y, tr] = afun(x,wi,dataSize,imSize,tr,G,SENSEmap)

if strcmp(tr,'transp')

    %reshape to orig data size
    x1 = reshape(x(1:prod(dataSize)),dataSize);
    numcoils = dataSize(end);
    y = zeros([imSize, numcoils]);
    parfor ii = 1:numcoils
        %type I NUFFT
        y(:,:,ii) = reshape(G'*(wi.*x1(:,ii)),imSize);
    end
    y = y(:);
    y = conj(SENSEmap).*y;
    y = reshape(y,[prod(imSize),dataSize(end)]);
    y = sum(y,2);

else

    xt=SENSEmap.*repmat(x,[dataSize(end) 1]);
    xt=reshape(xt,[imSize dataSize(end)]);
    numcoils=dataSize(end);
    y1=zeros(dataSize);
    parfor ii = 1:numcoils
        xtall = xt(:,:,ii);
        %type II NUFFT
        y1(:,ii)=wi.*col(G*xtall(:));
    end
    y = y1(:);

end

function out = col(in)

out = in(:);
