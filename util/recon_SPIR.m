function [res,FLAG,RELRES,ITER,RESVEC,LSVEC] = recon_SPIR(data, imginit, G, SPIRiTop, nIter, lambda,wts,tol)

% reconstruct an image from non-Cartesian acquisitions, using SPIRiT

%data       =   initial k-space data (must be a double!)
%imginit    =   initial image
%niteri     =   number of lsqr iterations
%wi         =   density weights
%G          =   NUFFT object (set with Gnufft)
%SPIRiTop        =   SPIRiT operator

  
N = numel(imginit);         %number of image points
imSize = size(imginit);     %image size
dataSize = [size(data)];    %k-space data size
wts=wts(:);
lambda=double(lambda);
b = [data(:).*repmat(sqrt(wts),[length(data(:))/length(wts) 1]) ; zeros(prod(imSize),1)];
if ~isvar('tol')
    tol=1e-6;
end
addpath(genpath('~/Copy/research/SPIRiT/'));
[res,FLAG,RELRES,ITER,RESVEC,LSVEC] = lsqr(@(x,tflag)afun(x,wts,G,SPIRiTop,dataSize, imSize,lambda,tflag), b, tol, nIter,speye(N,N),speye(N,N), imginit(:));

res = reshape(res,imSize); 
rmpath(genpath('~/Copy/research/SPIRiT/'));


function [y, tflag] = afun(x,wts,G,SPIRiTop,dataSize,imSize,lambda,tflag)

if strcmp(tflag,'transp')
    
    x1 = reshape(x(1:prod(dataSize)),dataSize); 
    x2 = reshape(x(prod(dataSize)+1:end),imSize);
    y = zeros([imSize]);
    parfor ii = 1:dataSize(2)% loop over coils, get Type II NUFFT of each residual
        y(:,:,ii) = reshape(double(G'*(sqrt(wts).*x1(:,ii))),imSize(1:2));
    end
    y = y(:);
    % tack on the SPIRiT regularization
    y = y + col(sqrt(lambda)*(SPIRiTop'*x2));
    
else
     
    x = reshape(x,imSize);
    y1 = zeros(dataSize);
    parfor ii = 1:dataSize(2) % loop over coils, get Type I NUFFT of each coil image
        y1(:,ii) = sqrt(wts).*double(G*col(x(:,:,ii)));
    end
    % tack on the SPIRiT regularization
    y2 = sqrt(lambda)*(SPIRiTop*x);
    y = [y1(:); y2(:)];
    
end

