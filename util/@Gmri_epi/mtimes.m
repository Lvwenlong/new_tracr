function res = mtimes(a,bb)

if streq(a.oneDorTwoD,'2D')
    if a.adjoint
        % k -> image, via ifft
        kmat = zeros(size(a.kmask));
        kmat(a.kmask) = bb;
        kmat = fftshift(kmat);
        
        fM2 = zeros(a.M,a.N/(2*a.Nsh),2*a.Nsh);
        for ii = 1:2*a.Nsh
            if a.shotMask(mod(ii-1,a.Nsh)+1) % skip operations if shot was skipped.
                % extract and DFT data for this segment
                % go back to image domain; undo ifft2 weighting
                fM2(:,:,ii) = ifft2(kmat(:,ii:2*a.Nsh:end))*(a.M*a.N/(2*a.Nsh));
                % compensate k-space shift (since all ifft's assume that data starts at
                % DC). We also apply k-space delays and phase shifts at this step
                fM2(:,:,ii) = bsxfun(@times,fM2(:,:,ii),conj(a.pePhs(ii,:)));
                fM2(:,:,ii) = bsxfun(@times,conj(a.fePhs(:,ii)),fM2(:,:,ii));
            end
        end
        res = fftshift(reshape(col(reshape(fM2,[a.M*a.N/(2*a.Nsh) 2*a.Nsh])*a.C),[a.M a.N]));
        
        if isfield(a,'immask');
            res = res(a.immask);
        else
            res = res(:);
        end
    else
        % image -> k, via fft
        tmp = zeros(numel(a.immask),1);
        tmp(a.immask) = bb;
        bb = reshape(tmp,[a.M a.N]);
        
        fM3 = reshape(reshape(fftshift(bb),[a.M*a.N/(2*a.Nsh) 2*a.Nsh])*a.C',...
            [a.M a.N/(a.Nsh*2) a.Nsh*2]);
        % (apply conjugate delay and phase shifts here)
        res = zeros(a.M,a.N);
        for ii = 1:2*a.Nsh
            if a.shotMask(mod(ii-1,a.Nsh)+1) % skip operations if shot was skipped.
                % un-compensate k-space shifts
                fM3(:,:,ii) = bsxfun(@times,a.fePhs(:,ii),fM3(:,:,ii));
                fM3(:,:,ii) = bsxfun(@times,fM3(:,:,ii),a.pePhs(ii,:));
                %fM3(:,:,ii) = fM3(:,:,ii).*(a.fePhs(:,ii)*a.pePhs(ii,:));
                % go back to Fourier domain and
                % interleave it with rest of k-space
                res(:,ii:2*a.Nsh:end) = fft2(fM3(:,:,ii));
            end
        end
        
        res = fftshift(res);
        res = res(a.kmask);
    end
else % 1D
    if a.adjoint
        % k -> image, via ifft
        kmat = zeros(size(a.kmask));
        kmat(a.kmask) = bb;
        kmat = fftshift(kmat,2);
        
        fM2 = zeros(a.M,a.N/(2*a.Nsh),2*a.Nsh);
        for ii = 1:2*a.Nsh
            if a.shotMask(mod(ii-1,a.Nsh)+1) % skip operations if shot was skipped.
                % extract and DFT data for this segment
                % go back to image domain; undo ifft2 weighting
%                 fM2(:,:,ii) = ifft(kmat(:,ii:2*a.Nsh:end),[],2)*(a.M*a.N/(2*a.Nsh));
                fM2(:,:,ii) = ifft(kmat(:,ii:2*a.Nsh:end),[],2)*(a.N/(2*a.Nsh));%*sqrt(a.M);
                % compensate k-space shift (since all ifft's assume that data starts at
                % DC). We also apply k-space delays and phase shifts at this step
                %fM2(:,:,ii) = bsxfun(@times,fM2(:,:,ii),conj(a.pePhs(ii,:)));
                %fM2(:,:,ii) = bsxfun(@times,conj(a.fePhs(:,ii)),fM2(:,:,ii));
            end
        end
        fM2 = fM2.*conj(a.allPhs);
        res = ifftshift(reshape(col(reshape(fM2,[a.M*a.N/(2*a.Nsh) 2*a.Nsh])*a.C),[a.M a.N]),2);
        
        if isfield(a,'immask');
            res = res(a.immask);
        else
            res = res(:);
        end
    else
        % image -> k, via fft
        tmp = zeros(numel(a.immask),1);
        tmp(a.immask) = bb;
        bb = reshape(tmp,[a.M a.N]);
        
        fM3 = reshape(reshape(fftshift(bb,2),[a.M*a.N/(2*a.Nsh) 2*a.Nsh])*a.C',...
            [a.M a.N/(a.Nsh*2) a.Nsh*2]);
        % (apply conjugate delay and phase shifts here)
        res = zeros(a.M,a.N);
        fM3 = fM3.*a.allPhs;
        for ii = 1:2*a.Nsh
            if a.shotMask(mod(ii-1,a.Nsh)+1) % skip operations if shot was skipped.
                % un-compensate k-space shifts
                %fM3(:,:,ii) = bsxfun(@times,a.fePhs(:,ii),fM3(:,:,ii));
                %fM3(:,:,ii) = bsxfun(@times,fM3(:,:,ii),a.pePhs(ii,:));
                %fM3(:,:,ii) = fM3(:,:,ii).*(a.fePhs(:,ii)*a.pePhs(ii,:));
                % go back to Fourier domain and
                % interleave it with rest of k-space
                res(:,ii:2*a.Nsh:end) = fft(fM3(:,:,ii),[],2);%*sqrt(a.M);
            end
        end
        
        res = fftshift(res,2);
        res = res(a.kmask);
    end
end

