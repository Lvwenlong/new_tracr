function sz = size(a)

sz = [sum(a.kmask(:)) numel(a.kmask)];
